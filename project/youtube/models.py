
from django.db import models

# Create your models here.
class VideosYT(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    titulo = models.CharField(max_length=256)
    link = models.TextField(max_length=256)
    descripcion = models.CharField(max_length=256)
    fecha = models.CharField(max_length=256)
    foto = models.CharField(max_length=256)
    canal = models.CharField(max_length=256)
    urlcanal = models.CharField(max_length=256)

    selected = models.BooleanField(default=False)  #Para los seleccionados

    def __str__(self):
        return self.id + " : " + self.titulo + " -> " + self.link

