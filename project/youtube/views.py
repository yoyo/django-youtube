"""Views for YouTube app
"""
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.middleware.csrf import get_token

from .ytchannel import YTChannel
from . import data


def build_html(name, list, action, token):    #Recorrerse todos los videos, inicializar la plantilla con los videos

    html = ""
    for video in list:
        html = html + data.VIDEO.format(link=video['link'],
                                   title=video['title'],
                                   id=video['id'],
                                   fecha=video['fecha'],  #Introducimos los nuevos elementos
                                   imagen=video['imagen'],
                                   descripcion=video['descripcion'],
                                   LinkCanal=video['LinkCanal'],
                                   TituloCanal=video['TituloCanal'],
                                   name=name,
                                   action=action,
                                   token=token)
    return html


def move_video(from_list, to_list, id):

    found = None
    for i, video in enumerate(from_list):
        if video['id'] == id:
            found = from_list.pop(i)
    if found:
        to_list.append(found)


def find_video(id, lista_1, lista_2):

    found = None
    for i, video in enumerate(lista_1):
        if video['id'] == id:
            found = video
    for i, video in enumerate(lista_2):
        if video['id'] == id:
            found = video
    return found


def main(request):
    if request.method == 'POST':
        if 'id' in request.POST:
            if request.POST.get('select'):
                move_video(from_list=data.selectable,
                           to_list=data.selected,
                           id=request.POST['id'])
            elif request.POST.get('deselect'):
                move_video(from_list=data.selected,
                           to_list=data.selectable,
                           id=request.POST['id'])
    csrf_token = get_token(request)
    selected = build_html(name='deselect', list=data.selected,
                          action='Deselect', token=csrf_token)
    selectable = build_html(name='select', list=data.selectable,
                            action='Select', token=csrf_token)
    htmlBody = data.PAGE.format(selected=selected,
                                selectable=selectable)
    return HttpResponse(htmlBody)

def get_Videos(request, id):
    found = None
    if request.method == 'GET':
        for i, video in enumerate(data.selected):
            if video['id'] == id:
                found = video
        if found != None:
            htmlBody = data.PAGE_VIDEOS.format(video=found)
            return HttpResponse(htmlBody)
    htmlBody = data.ERROR.format(id=id)
    return HttpResponse(htmlBody)


