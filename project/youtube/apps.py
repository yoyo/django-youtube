import urllib.request

from django.apps import AppConfig     #Mecanismo para saber como va a funcionar tu aplicacion
from .ytchannel import YTChannel
from . import data


class YouTubeConfig(AppConfig):
    name = 'youtube'

    def ready(self):     #Clave para que sepa la aplicacion el canal de youtube
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        channel = YTChannel(xmlStream)
        data.selectable = channel.videos()
