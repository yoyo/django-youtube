"""Data to keep between HTTP requests (app state)

* selected: list of selected videos
* selectable: list of selectable videos
"""

selected = []
selectable = []

PAGE = """     
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Django YouTube (version 1)</h1>
    <h2>Selected</h2>
      <ul>
      {selected}
      </ul>
    <h2>Selectable</h2>
      <ul>
      {selectable}
      </ul>
  </body>
</html>
"""
PAGE_VIDEOS = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div align="center">
    <h1>Canal del video: <a href='{video[LinkCanal]}'>{video[TituloCanal]}</a></h1>
    <h2><a href='{video[link]}'>{video[title]}</a></h2>
        <br><a href='{video[link]}'><img src={video[imagen]}></a>
        <br>Fecha de publicación: {video[fecha]}
        </div>
        <p>Descripcion: {video[descripcion]}</p>
    <h3><a href='http://localhost:1234/youtube'>PAGINA PRINCIPAL</a></h3>
  </body>
</html>
"""
ERROR = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <h3>El identificador {id} no es de la lista de los videos</h3>
    </body>
</html>
"""

VIDEO = """
      <li>
        <form action='/' method='post'>
          <a href='{link}'>{title}</a>
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'> 
          <input type='submit' value='{action}'>
        </form>
      </li>
"""
