"""Youtube app resource configuration
"""
from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='main'),
    path('<str:id>', views.get_Videos, name='video'),
]
